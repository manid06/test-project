const fs = require('fs')
const jsonData = require('../data.json')
const md5 = require('md5')
const { json } = require('body-parser')
const { rejects } = require('assert')

//creating a user
function createUser(data){
    return new Promise((resolve, reject)=>{
        try{
            jsonData.forEach((user,index)=>{
                if(user.email == data.email){
                    reject({'msg': "sorry this email is already registered"})
                }
                if(index == jsonData.length - 1 ){
                    //hashing the password
                    data.password = md5(data.password)
                    jsonData.push({'msg': "user registered successfully"})
                }

            })
            resolve(data)
    
        }catch(e){
            reject(e)
        }
    })
}

function editUser(email, data){
    return new Promise((resolve, reject)=>{
            try{

                jsonData.forEach((user, index)=>{
                    if(user.email === email){
                        data.mobile?jsonData[index].mobile = data.mobile:''
                        data.name? jsonData[index.name]= data.name:'' 
                        resolve(data)
                    }
                    if(index == jsonData.length - 1)
                        reject({'msg': 'sorry, email id not found in our records'})
                    
                })
        
            }catch(e){
                reject(e)
            }
        })

}

function changeUserPassword(email, password){
    return new Promise((resolve, reject)=>{
            try{
                jsonData.forEach((user, index)=>{
                    if(user.email === email){
                        data.password?jsonData[index].password = md5(data.password):'' 
                        resolve(data)
                    }
                    if(index == jsonData.length-1)
                        reject({'msg': 'sorry, email id not found in our records'})
                    
                })
        
            }catch(e){
                reject(e)
            }
        })

}


function verifyUser(email,password){
    return new Promise((resolve, reject)=>{
            try{
                jsonData.forEach((user, index)=>{
                    if(user.email == email && user.password == password){
                        resolve(true)
                    }
                    if(index == jsonData.length -1)
                        reject(false)
                })
        
            }catch(e){
                reject(e)
            }
        })
}


module.exports = {
    createUser,
    editUser,
    verifyUser,
    changeUserPassword
}