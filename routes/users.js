const { rejects } = require('assert')

const Router = require('express').Router()
const userCtrl = require('../controllers/users')

Router.post('/login', (req,res)=>{
    try{
       userCtrl.verifyUser(req.body.email, req.body.password).then(response=>{
           res.status(200).json({'msg': 'logged in successfully'})
       })

    }catch(err){
        res.status(400).json(err)
    }
})


//updating the user
Router.put('/:email', (req,res)=>{
    try{
        userCtrl.createUser(req.params.email, req.body).then(data=>{
            res.status(200).json({'msg': 'user updated'})
        })

    }catch(err){
        res.status(400).json(err)
    }
})

//create the user
Router.post('/register', (req,res)=>{
    try{
        userCtrl.createUser(req.body).then(data=>{
            res.status(200).json(data)
        })

    }catch(err){
        res.status(400).json(err)
    }
})





module.exports = Router