const express = require('express')
const mongoose = require('mongoose')
const app = express()




//importing routes here
const userRoutes = require('./routes/users')

const bodyParser = require('body-parser')


const port = 3000

app.use(express.json())
app.use(bodyParser({extended: true}))


//server starting on port 3000
app.listen(port, ()=>{
    console.log('server started on port', port)
})

//using routes in our app
app.use('/user', userRoutes)



