const mongoose = require('mongoose')
const userSchema = mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String
    },
    password: String,
    mobile: String


})

module.exports = mongoose.model('users', userSchema)